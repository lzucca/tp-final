# Usamos ubuntu como imagen base
FROM ubuntu:18.04

# Instalacion de paquetes
RUN apt-get update && apt-get upgrade -y && apt-get install -y git curl
        
# Instalar NodeJS
RUN curl -fsSL https://deb.nodesource.com/setup_14.x | bash - && apt-get install -y nodejs

# Se define el directorio de trabajo
WORKDIR /usr/src/app

# Se clona el proyecto de Angular, se instalan los paquetes necesarios para el proyecto
RUN git clone https://gitlab.com/lzucca/pokedex.git
RUN cd /usr/src/app/pokedex && npm install

# Se hace el build de la app
RUN cd /usr/src/app/pokedex && npm run build

# A partir de una imagen de NGINX, se publica el distribuible de nuestra app en el servidor web
FROM nginx:1.17.1-alpine
COPY --from=build /usr/src/app/pokedex/dist/ng-pokedex /usr/share/nginx/html
